﻿Imports System.Data.SqlClient
Imports logica

Public Class ListarAlumnos
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Close()
    End Sub

    Private Sub ListarAlumnos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
        Try
            dgwAlumnos.DataSource = Sistema.GetInstance.TraerTodoBD()
            dgwAlumnos.AutoResizeColumns()
            dgwAlumnos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        Catch ex As SqlException
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class