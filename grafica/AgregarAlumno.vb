﻿Imports System.Data.SqlClient
Imports logica

Public Class AgregarAlumno
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.Close()
    End Sub

    Private Sub AgregarAlumno_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
        cbxTurno.Items.Add("Matutino")
        cbxTurno.Items.Add("Vespertino")
        cbxTurno.Items.Add("Nocturno")
        cbxTurno.SelectedIndex = 0
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        'Formato de la fecha que se deberia de enviar a la BD 1999-10-30
        'Esto es debido a que el SQL Server está en ingles, madre mia keibrons
        'INSTALEN EL SQL SERVER EN ESPAÑOL HIJOS DE PUTEN, GRACIAS
        Dim fecha As Date = dtpFecha.Value.ToShortDateString().Trim()
        Dim resultado As Boolean = False
        Dim ci As Integer = txtCi.Text.Trim()
        Dim nombreC As String = txtNombre.Text.Trim()
        Dim apellidoC As String = txtApellido.Text.Trim()
        Dim institucion As String = txtInstitucion.Text.Trim()
        Dim clase As String = txtClase.Text.Trim()
        Dim turno As String = cbxTurno.SelectedItem.ToString()
        Dim email As String = txtEmail.Text.Trim()
        Try
            Sistema.GetInstance.ConectarBD()
            resultado = Sistema.GetInstance.InsertarEnBd(ci, nombreC, apellidoC, fecha, institucion, clase, turno, email)
            If resultado Then
                MessageBox.Show("Se inserto correctamente en la base de datos")
                Sistema.GetInstance.DesconectarBD()
                txtApellido.Text = ""
                txtCi.Text = ""
                txtClase.Text = ""
                txtEmail.Text = ""
                txtInstitucion.Text = ""
                txtNombre.Text = ""
            Else
                MessageBox.Show("Error al insertar en la base de datos")
                Sistema.GetInstance.DesconectarBD()
            End If
        Catch ex As SqlException
            MessageBox.Show(ex.Message)
            Sistema.GetInstance.DesconectarBD()
        Catch ex As ArgumentException
            MessageBox.Show(ex.Message)
            Sistema.GetInstance.DesconectarBD()
        End Try

    End Sub
End Class