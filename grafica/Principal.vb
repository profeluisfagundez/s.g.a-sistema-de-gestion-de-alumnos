﻿Imports System.Data.SqlClient
Imports logica

Public Class Principal
    Private Sub Principal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Fecha del día tomada del sistema operativo
        Dim thisDate As Date
        thisDate = Today
        lblFechaActual.Text = thisDate
        Me.CenterToScreen()
        'Cumpleaños de alumnos 
        Try
            dgvCumples.DataSource = Sistema.GetInstance.RetorntarCumpleFachada()
            dgvCumples.AutoResizeColumns()
            dgvCumples.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        Catch ex As SqlException
            MessageBox.Show(ex.Message)
        End Try
        'Cantidad de alumnos almacenados en la base de datos
        Sistema.GetInstance.DesconectarBD()
        Sistema.GetInstance.ConectarBD()
        Dim tabla As DataTable = Sistema.GetInstance.CantidadDeAlumnos()
        Dim valor As Integer = tabla.Rows.Count
        lblCantAlumnos.Text = valor
    End Sub

    Private Sub AgregarAlumnoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AgregarAlumnoToolStripMenuItem.Click
        AgregarAlumno.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Dim resultado As Integer = MessageBox.Show("¿Está seguro que desea salir?", "S.G.A", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If resultado = DialogResult.No Then
            MessageBox.Show("Volviendo a S.G.A", "S.G.A")
        ElseIf resultado = DialogResult.Yes Then
            MessageBox.Show("Hasta luego", "S.G.A")
            Me.Close()
        End If
    End Sub

    Private Sub BorrarAlumnoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BorrarAlumnoToolStripMenuItem.Click
        BorrarAlumno.Show()
    End Sub

    Private Sub CreditosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CreditosToolStripMenuItem.Click
        Creditos.Show()
    End Sub

    Private Sub ListarAlumnosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListarAlumnosToolStripMenuItem.Click
        ListarAlumnos.Show()
    End Sub

    Private Sub ModificarAlumnosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModificarAlumnosToolStripMenuItem.Click
        ModificarAlumno.Show()
    End Sub

    Private Sub btnRefrescar_Click(sender As Object, e As EventArgs) Handles btnRefrescar.Click
        Application.Restart()
    End Sub

End Class
