﻿Imports logica

Public Class BorrarAlumno
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.Close()
    End Sub

    Private Sub BorrarAlumno_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Sistema.GetInstance.DesconectarBD()
        Sistema.GetInstance.ConectarBD()
        Try
            Dim ci As String = txtCi.Text.Trim
            Dim existe As Boolean
            Dim tabla As DataTable = Sistema.GetInstance.ExisteAlumno(ci)
            For Each fila As DataRow In tabla.Rows
                If fila.ItemArray.Contains(1) Then
                    existe = True
                End If
            Next
            If existe Then
                Dim resultado As Integer = MessageBox.Show("El alumno existe, está seguro que desea borrarlo?", "S.G.A", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If resultado = DialogResult.No Then
                    MessageBox.Show("Se cancelo la operación", "S.G.A")
                    Sistema.GetInstance.DesconectarBD()
                Else
                    Sistema.GetInstance.ConectarBD()
                    Dim borrar As Boolean = Sistema.GetInstance.borrarAlumnoBD(ci)
                    Sistema.GetInstance.DesconectarBD()
                    MessageBox.Show("Se borraron los datos", "S.G.A")
                End If
            Else
                MessageBox.Show("Error, no existe el alumno", "S.G.A")
                Sistema.GetInstance.DesconectarBD()
            End If
        Catch ex As InvalidCastException
            MessageBox.Show(ex.Message)
            Sistema.GetInstance.DesconectarBD()
        End Try
    End Sub

End Class