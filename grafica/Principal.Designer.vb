﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Principal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgregarAlumnoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListarAlumnosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BorrarAlumnoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarAlumnosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreditosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblFechaActual = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dgvCumples = New System.Windows.Forms.DataGridView()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblCantAlumnos = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.btnRefrescar = New System.Windows.Forms.Button()
        Me.MenuStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvCumples, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.CreditosToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(784, 29)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AgregarAlumnoToolStripMenuItem, Me.ListarAlumnosToolStripMenuItem, Me.BorrarAlumnoToolStripMenuItem, Me.ModificarAlumnosToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(75, 25)
        Me.ArchivoToolStripMenuItem.Text = "Archivo"
        '
        'AgregarAlumnoToolStripMenuItem
        '
        Me.AgregarAlumnoToolStripMenuItem.Name = "AgregarAlumnoToolStripMenuItem"
        Me.AgregarAlumnoToolStripMenuItem.Size = New System.Drawing.Size(212, 26)
        Me.AgregarAlumnoToolStripMenuItem.Text = "Agregar alumnos"
        '
        'ListarAlumnosToolStripMenuItem
        '
        Me.ListarAlumnosToolStripMenuItem.Name = "ListarAlumnosToolStripMenuItem"
        Me.ListarAlumnosToolStripMenuItem.Size = New System.Drawing.Size(212, 26)
        Me.ListarAlumnosToolStripMenuItem.Text = "Listar alumnos"
        '
        'BorrarAlumnoToolStripMenuItem
        '
        Me.BorrarAlumnoToolStripMenuItem.Name = "BorrarAlumnoToolStripMenuItem"
        Me.BorrarAlumnoToolStripMenuItem.Size = New System.Drawing.Size(212, 26)
        Me.BorrarAlumnoToolStripMenuItem.Text = "Borrar Alumnos"
        '
        'ModificarAlumnosToolStripMenuItem
        '
        Me.ModificarAlumnosToolStripMenuItem.Name = "ModificarAlumnosToolStripMenuItem"
        Me.ModificarAlumnosToolStripMenuItem.Size = New System.Drawing.Size(212, 26)
        Me.ModificarAlumnosToolStripMenuItem.Text = "Modificar Alumnos"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(212, 26)
        Me.SalirToolStripMenuItem.Text = "Salir"
        '
        'CreditosToolStripMenuItem
        '
        Me.CreditosToolStripMenuItem.Name = "CreditosToolStripMenuItem"
        Me.CreditosToolStripMenuItem.Size = New System.Drawing.Size(80, 25)
        Me.CreditosToolStripMenuItem.Text = "Creditos"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblFechaActual)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(562, 32)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(210, 100)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Fecha de hoy en Uruguay"
        '
        'lblFechaActual
        '
        Me.lblFechaActual.AutoSize = True
        Me.lblFechaActual.Location = New System.Drawing.Point(7, 53)
        Me.lblFechaActual.Name = "lblFechaActual"
        Me.lblFechaActual.Size = New System.Drawing.Size(33, 20)
        Me.lblFechaActual.TabIndex = 0
        Me.lblFechaActual.Text = "......"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvCumples)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(22, 138)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(750, 170)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Cumpleaños"
        '
        'dgvCumples
        '
        Me.dgvCumples.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCumples.Location = New System.Drawing.Point(7, 25)
        Me.dgvCumples.Name = "dgvCumples"
        Me.dgvCumples.Size = New System.Drawing.Size(737, 139)
        Me.dgvCumples.TabIndex = 0
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblCantAlumnos)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(22, 32)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(176, 100)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Cantidad de alumnos almacenados"
        '
        'lblCantAlumnos
        '
        Me.lblCantAlumnos.AutoSize = True
        Me.lblCantAlumnos.Location = New System.Drawing.Point(6, 53)
        Me.lblCantAlumnos.Name = "lblCantAlumnos"
        Me.lblCantAlumnos.Size = New System.Drawing.Size(33, 20)
        Me.lblCantAlumnos.TabIndex = 0
        Me.lblCantAlumnos.Text = "......"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(204, 32)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(207, 100)
        Me.GroupBox4.TabIndex = 5
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Cantidad de instituciones almacenadas"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 20)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "......"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.btnRefrescar)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(417, 32)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(146, 100)
        Me.GroupBox5.TabIndex = 6
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Refrescar"
        '
        'btnRefrescar
        '
        Me.btnRefrescar.Location = New System.Drawing.Point(6, 36)
        Me.btnRefrescar.Name = "btnRefrescar"
        Me.btnRefrescar.Size = New System.Drawing.Size(133, 43)
        Me.btnRefrescar.TabIndex = 0
        Me.btnRefrescar.Text = "Refrescar"
        Me.btnRefrescar.UseVisualStyleBackColor = True
        '
        'Principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 561)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Principal"
        Me.Text = "S.G.A - Sistema de Gestión de Alumnos"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvCumples, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ArchivoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AgregarAlumnoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListarAlumnosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BorrarAlumnoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ModificarAlumnosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CreditosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lblFechaActual As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents lblCantAlumnos As Label
    Friend WithEvents dgvCumples As DataGridView
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents Label2 As Label
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents btnRefrescar As Button
End Class
