﻿Imports System.Data.SqlClient

Public Class Persistencia

    'Programador Luis Eduardo Fagúndez
    'OBJETOS IMPORTANTES DE ADO.NET
    'SqlConnection = Manejara la conexión a la base de datos, es necesario pasarle la cadena de conexión 
    'DataSet = Representa una base de datos de manera virtual (en memoria), puede tener tablas, columnas, filas etc.
    'DataTable = Representa una tabla de la base de datos en memoria
    'SqlDataAdapter = sirve como puente entre SQL Server y el DataSet.
    'SqlCommand = Sirve para generar la instrucción de SQL junto a la conexión que se encuentre activa

    Public Property Conexion As SqlConnection
    Public Property DS As DataSet
    Public Property DT As DataTable
    Public Property Adaptador As SqlDataAdapter
    Public Property Comando As SqlCommand

    'En el constructor de la clase generamos la instancia de la propiedad "Conexion" y le
    'pasamos la cadena de conexión de la base de datos
    Public Sub New()
        Conexion = New SqlConnection With {
            .ConnectionString = "Server=DESKTOP-IMSLO0Q; database=SGA; trusted_connection=yes"
        }
    End Sub

    'Método que permite coenctar la base de datos
    Public Sub Conectar()
        Conexion.Open()
    End Sub

    'Método que permite descoenctar la base de datos
    Public Sub Desconectar()
        Conexion.Close()
    End Sub

    'El método InsertarBD() recibe de la ventana Principal todos los datos para la construcción del objeto en la capa de negocio, 
    'Este método armará la cadena de conexión para ejecutar la consulta, gracias a que el método es del tipo Boolean se puede saber si la inserción de datos
    'tuvo éxito o no debido a que devuelve verdadero o falso.
    'Es necesario controlar una capa más arriba la excepción SqlException, debido a que puede dar problemas.
    Public Function InsertarBD(ByVal ci As Integer, ByVal nombre As String, ByVal apellido As String, ByVal fechaN As Date, ByVal instituto As String, ByVal grupo As String, ByVal turno As String, ByVal email As String) As Boolean
        Dim comandoSql As SqlCommand
        Dim cadena As String = "INSERT INTO alumnos VALUES(" + CStr(ci) + "," + "'" + nombre + "'" + "," + "'" + apellido + "'" + "," + "'" + fechaN.ToShortDateString + "'" + "," + "'" + instituto + "'" + "," + "'" + grupo + "'" + "," + "'" + turno + "'" + "," + "'" + email + "'" + ")"
        Dim resultado As Integer
        comandoSql = New SqlCommand(cadena, Conexion)
        resultado = comandoSql.ExecuteNonQuery
        If resultado > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    'Método que permite traer todos los registros de la base de datos y guardarlos en un tabla virtual
    'Es necesario controlar una capa más arriba la excepción SqlException, debido a que puede dar problemas.
    Public Function TraerRegistrosBD() As DataTable
        DT = New DataTable
        Dim cadena As String = "SELECT * FROM alumnos"
        Adaptador = New SqlDataAdapter(cadena, Conexion)
        Adaptador.Fill(DT)
        Return DT
    End Function

    'Método que permite traer todos los alumnos que cumplen años, la fecha se saca del sistema operativo.
    Public Function RetornarCumple() As DataTable
        Conectar()
        Dim fechaN As Date = Today
        Dim fechaFinal As String = fechaN.Year.ToString + "-" + fechaN.Month.ToString + "-" + fechaN.Day.ToString
        DT = New DataTable
        Dim cadena As String = "SELECT ci,nombre_completo,apellido_completo,clase,institucion FROM alumnos WHERE fecha_de_nacimiento = " + "'" + fechaFinal + "'"
        Adaptador = New SqlDataAdapter(cadena, Conexion)
        Adaptador.Fill(DT)
        Desconectar()
        Return DT
    End Function

    'Método que permite borrar un alumno de la base de datos, éste método se comunica con la fachada.
    Public Function BorrarAlumno(ci As String) As Boolean
        Dim resultado As Boolean
        Dim cadena = "DELETE FROM alumnos WHERE ci='" & ci & "'"
        Comando = New SqlCommand(cadena, Conexion)
        resultado = Comando.ExecuteNonQuery
        Return resultado
    End Function

    'Método que retorna un DataTable con un valor entero que nos indicara si existe o no un usuario en BD
    Public Function ExisteAlumno(ci As String) As DataTable
        DT = New DataTable
        Dim cadena As String = "SELECT COUNT(ci) FROM alumnos WHERE ci='" & ci & "'"
        Adaptador = New SqlDataAdapter(cadena, Conexion)
        Adaptador.Fill(DT)
        Desconectar()
        Return DT
    End Function

    'Método que retorna un DataTable con los alumnos que están almacenados en la Basé de datos
    Public Function CantidadDeAlumnos() As DataTable
        DT = New DataTable
        Dim cadena As String = "SELECT ci FROM alumnos"
        Adaptador = New SqlDataAdapter(cadena, Conexion)
        Adaptador.Fill(DT)
        Desconectar()
        Return DT
    End Function

End Class