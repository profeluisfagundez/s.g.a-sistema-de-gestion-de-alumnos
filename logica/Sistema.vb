﻿Imports peristencia

Public Class Sistema

    'Autor: Luis Eduardo Fagúndez
    'La clase Sistema cumple como Fachada dentro de nuestra aplicación
    Private Shared _objeto As Sistema = Nothing
    Private BD As Persistencia

    Private Sub New()
        BD = New Persistencia()
    End Sub

    'Método GetInstance()
    'Devuelve una instancia de la clase Sistema, si el objeto no está instanciado, lo hace y lo devuelve
    Public Shared Function GetInstance() As Sistema
        If _objeto Is Nothing Then
            _objeto = New Sistema()
        End If
        Return _objeto
    End Function

    'Realiza una conección a la base de datos
    Public Sub ConectarBD()
        BD.Conectar()
    End Sub

    'Termina la conexión a la base de datos
    Public Sub DesconectarBD()
        BD.Desconectar()
    End Sub

    'Método que inserta datos en la BD, hace uso de la capa de persistencia
    'Es necesario controlar una capa más arriba la excepción SqlException, debido a que puede dar problemas.
    Public Function InsertarEnBd(ByVal ci As Integer, ByVal nombre As String, ByVal apellido As String, ByVal fechaN As Date, ByVal instituto As String, ByVal grupo As String, ByVal turno As String, ByVal email As String) As Boolean
        Dim resultado As Boolean = False
        resultado = BD.InsertarBD(ci, nombre, apellido, fechaN, instituto, grupo, turno, email)
        If resultado Then
            resultado = True
        End If
        Return resultado
    End Function

    'Retorna un DataTable con un valor número, dicho valor devuelve si existe o no el alumno en BD
    Public Function ExisteAlumno(ci As String) As DataTable
        Return BD.ExisteAlumno(ci)
    End Function

    Public Function CantidadDeAlumnos() As DataTable
        Return BD.CantidadDeAlumnos()
    End Function

    'Retorna los registros de la BD que están en la tabla alumnos
    'Es necesario controlar una capa más arriba la excepción SqlException, debido a que puede dar problemas.
    Public Function TraerTodoBD() As DataTable
        Return BD.TraerRegistrosBD()
    End Function

    Public Function RetorntarCumpleFachada()
        Return BD.RetornarCumple()
    End Function

    Public Function borrarAlumnoBD(ci As String) As Boolean
        Dim resultado As Boolean
        resultado = BD.BorrarAlumno(ci)
        Return resultado
    End Function

End Class
