﻿Public Class Alumno

    Public Property Ci As Integer
    Public Property Nombre As String
    Public Property Apellido As String
    Public Property FechaDeNacimiento As Date
    Public Property Institucion As String
    Public Property Clase As String
    Public Property Turno As String
    Public Property Email As String

    Public Sub New(ci As Integer, nombre As String, apellido As String, fechaNacimiento As Date, institucion As String, clase As String, turno As String, email As String)
        Me.Ci = ci
        Me.Nombre = nombre
        Me.Apellido = apellido
        FechaDeNacimiento = fechaNacimiento
        Me.Institucion = institucion
        Me.Clase = clase
        Me.Turno = turno
        Me.Email = email
    End Sub

    'Método ToString()
    'Devuelve una tupla de datos con todos los get de un objeto de la clase Alumno.
    Public Overrides Function ToString() As String
        Return "CI: " & Ci & " Nombre: " & Nombre & " Apellido: " & Apellido & " Fecha de nacimiento: " & FechaDeNacimiento & " Institución: " & Institucion & " Clase: " & Clase & " Turno: " & Turno & " Email: " & Email
    End Function

    'Método Equals()
    'Recibe un Object como párametro y se le aplica un Cast a Alumno.
    'Devuelve si el objeto recibido es igual a otro del mismo tipo de Clase.
    Public Overrides Function Equals(obj As Object) As Boolean
        Dim valido As Boolean = False
        If TypeOf obj Is Alumno Then
            Dim estudiante As Alumno
            estudiante = DirectCast(obj, Alumno)
            If estudiante.Ci = Me.Ci Then
                valido = True
            End If
        End If
        Return valido
    End Function

End Class
