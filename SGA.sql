USE [master]
GO
/****** Object:  Database [SGA]    Script Date: 2018/12/18 13:41:37 ******/
CREATE DATABASE [SGA]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SGA', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\SGA.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SGA_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\SGA_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [SGA] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SGA].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SGA] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SGA] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SGA] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SGA] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SGA] SET ARITHABORT OFF 
GO
ALTER DATABASE [SGA] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SGA] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SGA] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SGA] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SGA] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SGA] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SGA] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SGA] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SGA] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SGA] SET  ENABLE_BROKER 
GO
ALTER DATABASE [SGA] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SGA] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SGA] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SGA] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SGA] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SGA] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SGA] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SGA] SET RECOVERY FULL 
GO
ALTER DATABASE [SGA] SET  MULTI_USER 
GO
ALTER DATABASE [SGA] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SGA] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SGA] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SGA] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SGA] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SGA', N'ON'
GO
ALTER DATABASE [SGA] SET QUERY_STORE = OFF
GO
USE [SGA]
GO
/****** Object:  Table [dbo].[alumnos]    Script Date: 2018/12/18 13:41:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[alumnos](
	[ci] [int] NOT NULL,
	[nombre_completo] [varchar](50) NOT NULL,
	[apellido_completo] [varchar](50) NOT NULL,
	[fecha_de_nacimiento] [date] NOT NULL,
	[institucion] [varchar](30) NOT NULL,
	[clase] [varchar](30) NOT NULL,
	[turno] [varchar](30) NOT NULL,
	[email] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ci] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[alumnos] ([ci], [nombre_completo], [apellido_completo], [fecha_de_nacimiento], [institucion], [clase], [turno], [email]) VALUES (99999999, N'9', N'9', CAST(N'2018-11-30' AS Date), N'9', N'9', N'Matutino', N'9')
GO
INSERT [dbo].[alumnos] ([ci], [nombre_completo], [apellido_completo], [fecha_de_nacimiento], [institucion], [clase], [turno], [email]) VALUES (33333333, N'asd', N'asd', CAST(N'2018-11-29' AS Date), N'asd', N'asdasd', N'Matutino', N'asdasdad')
GO
INSERT [dbo].[alumnos] ([ci], [nombre_completo], [apellido_completo], [fecha_de_nacimiento], [institucion], [clase], [turno], [email]) VALUES (56474744, N'asdasd', N'asdasd', CAST(N'2018-01-01' AS Date), N'asdasd', N'asdasd', N'Matutino', N'asdasd')
GO
INSERT [dbo].[alumnos] ([ci], [nombre_completo], [apellido_completo], [fecha_de_nacimiento], [institucion], [clase], [turno], [email]) VALUES (87456434, N'José Carlos', N'Barboza Herrera', CAST(N'2007-12-06' AS Date), N'ISBO', N'2ID', N'Matutino', N'Carlos@carlos.com')
GO
INSERT [dbo].[alumnos] ([ci], [nombre_completo], [apellido_completo], [fecha_de_nacimiento], [institucion], [clase], [turno], [email]) VALUES (11111111, N'Pepe', N'Fernandez', CAST(N'2000-12-31' AS Date), N'ISBO', N'4IC', N'Matutino', N'pepe@pepito.com')
GO
INSERT [dbo].[alumnos] ([ci], [nombre_completo], [apellido_completo], [fecha_de_nacimiento], [institucion], [clase], [turno], [email]) VALUES (48225577, N'Luis Eduardo', N'Fagúndez Pedrozo', CAST(N'1992-12-21' AS Date), N'ISBO', N'3IA', N'Matutino', N'profeluisfagundez@gmail.com')
GO
INSERT [dbo].[alumnos] ([ci], [nombre_completo], [apellido_completo], [fecha_de_nacimiento], [institucion], [clase], [turno], [email]) VALUES (87777777, N'asda', N'asdasd', CAST(N'2018-12-18' AS Date), N'asdasd', N'asdasd', N'Vespertino', N'asdasd@asdasd.com')
GO
USE [master]
GO
ALTER DATABASE [SGA] SET  READ_WRITE 
GO
