# S.G.A Sistema de Gesti�n de Alumnos
Agenda electronica de alumnos programanada en Visual Basic.NET 

Programador: Luis Eduardo Fag�ndez

Lenguajes utilizados: VB.NET y SQL para Base de Datos

Herramientas utilizadas: ADO.NET

�sta aplicaci�n fue creada para explicar los conceptos b�sicos del desarrollo de software en Visual Basic.NET. En esta aplicaci�n se trabajan los siguientes temas:

- POO
- Clases
- Herencia
- Polimorfismo
- ADO.NET
- Formularios
- Referencias
- Manejo de Capas
- Dise�o de patrones Facade and Singleton

�sta aplicaci�n fue desarrollada usando la licencia GPL3, queda totalmente prohibido su uso comerical. 
Gu�a r�pida de la licencia GPL versi�n 3: https://www.gnu.org/licenses/quick-guide-gplv3.html
